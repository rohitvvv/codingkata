
//Lower level class
class Worker {
    work() {
        console.log("I work hard");
    }
}

//Higher level class
//Manager has picked up dependency on Worker
class Manager {

    setWorker(worker) {
        this.worker = worker;
    }

    manage() {
        this.worker.work();
    }
}

//Yet another low level class.Future requirement.
//Cannot be introduced without making changes to the Manager!!!
class SuperWorker {
    work() {
        console.log("I am the super worker");
    }
}

(function () {
    var manager = new Manager();
    manager.setWorker(new Worker());
    manager.manage();
})();
