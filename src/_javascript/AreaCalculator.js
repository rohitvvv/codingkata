class Rectangle {
  constructor() {
    this.width = 0;
    this.height = 0;
  }

  setWidth(width) {
    this.width = width;
  }

  setHeight(height) {
    this.height = height;
  }

  getArea() {
    return this.width * this.height;
  }
}

class Square extends Rectangle {
  setWidth(width) {
    this.width = width;
    this.height = width;
  }

  setHeight(height) {
    this.width = height;
    this.height = height;
  }
}

(function() {
    var rectangle = new Rectangle();
    rectangle.setHeight(10);
    rectangle.setWidth(20);
    console.log(rectangle.getArea());

    var square = new Square();
    square.setHeight(10);
    console.log(square.getArea());
})();
