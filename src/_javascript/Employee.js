class Employee {

    constructor(firstName, lastName) {
        this.employeeId = '';
        this.firstName = firstName;
        this.lastName = lastName;
        this.experience;
        this.salary;

    }
    adjustSalary() {
        if (this.experience < 5) {
            this.salary = 2000;
        }
        if (this.experience > 5 && this.experience < 10) {
            this.salary = 5000;
        }
        if (this.experience < 15) {
            this.salary = 10000;
        }
    }

    getDesignation() {
        if (this.experience < 2) {
            return "D2";
        }
        if (this.experience < 5 && this.experience > 2) {
            return "D3";
        }
        if (this.experience > 10) {
            return "M1";
        }
        return "CEO";
    }

    setEmployeeId(x) {
        this.employeeId = x;
    }

    printEmployeeDetails() {
        console.log("This method will print employee details");
    }

    employeeMagicMethod() {
        return Math.random();
    }


}

(function () {    
    var employee = new Employee("Mohandas", "Karamchand", "Gandhi");
    employee.setEmployeeId(1);
})();