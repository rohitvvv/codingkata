
class LinkRenderer {
    renderLink(linkType) {
        switch (linkType) {
            case "LinkAndTextBelow":
                this.renderLinkAndTextBelow(new LinkAndTextBelow());
                break;
            case "LinkAndTextAdjacent":
                this.renderLinkAndTextAdjacent(new LinkAndTextAdjacent());
                break;
            default:
                console.log("Link Only");
        }
    }

    renderLinkAndTextBelow(link) {
        console.log("Link and Text Below");
    }

    renderLinkAndTextAdjacent(linkAndTextAdjacent) {
        console.log("Link And Text Adjacent");
    }
}

class Link {
    constructor(linkType) {
        this.linkType = linkType;
    }
}

class LinkAndTextBelow extends Link {
    constructor(link){
        super(link);
    }
    LinkAndTextBelow() {
        super.linkType = "LinkAndTextBelow";
    }
}

class LinkAndTextAdjacent extends Link {
    constructor(link){
        super(link);
    }
    LinkAndTextAdjacent() {
        super.linkType = "LinkAndTextAdjacent";
    }
}

(function(){

var linkrederer = new LinkRenderer();
linkrederer.renderLink("LinkAndTextBelow");

})();