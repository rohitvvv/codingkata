﻿using System;
namespace CodingKata
{
    class Worker
    {
        public void work()
        {
            Console.WriteLine("I work hard");
        }
    }

    //Higher level class
    //Manager has picked up dependency on Worker
    class Manager
    {
        Worker worker;

        public void setWorker(Worker worker)
        {
            this.worker = worker;
        }

        public void manage()
        {
            worker.work();
        }
    }

    //Yet another low level class.Future requirement.
    //Cannot be introduced without making changes to the Manager!!!
    class SuperWorker
    {
        public void work()
        {
            Console.WriteLine("I am the super worker");
        }
    }

    public class DependentWorker
    {
        public static void main(String[] args)
        {
            Manager manager = new Manager();
            manager.setWorker(new Worker());
            manager.manage();
        }
    }
}