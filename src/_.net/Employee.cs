﻿using System;

namespace CodingKata
{
    public class Employee
    {

        int employeeId;
        String firstName;
        String middleName;
        String lastName;
        int experience;
        int salary;

        public void calculateSalary()
        {
            if (experience < 5)
            {
                salary = 2000;
            }
            if (experience > 5 && experience < 10)
            {
                salary = 5000;
            }
            if (experience < 15)
            {
                salary = 10000;
            }
        }

        String getDesignation()
        {
            if (experience < 2)
            {
                return "D2";
            }
            if (experience < 5 && experience > 2)
            {
                return "D3";
            }
            if (experience > 10)
            {
                return "M1";
            }
            return "CEO";
        }

        Employee(String firstName, String middleName, String lastName)
        {
            this.firstName = firstName;
            this.middleName = middleName;
            this.lastName = lastName;
        }

        public void setEmployeeId(int x)
        {
            this.employeeId = x;
        }

        public void printEmployeeDetails()
        {
            Console.WriteLine("This method will print employee details");
        }

        public Double employeeMagicMethod()
        {
            Random rdm = new Random();
            return rdm.NextDouble();
        }

        public static void main(String[] args)
        {
            Employee employee = new Employee("Mohandas", "Karamchand", "Gandhi");
            employee.setEmployeeId(1);
            employee.experience = 10;
            Console.WriteLine(employee.getDesignation());
        }
    }
}