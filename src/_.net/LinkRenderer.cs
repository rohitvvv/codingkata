﻿using System;

namespace CodingKata
{
    public class LinkRenderer
    {
        void renderLink(String linkType)
        {
            switch (linkType)
            {
                case "LinkAndTextBelow":
                    renderLinkAndTextBelow(new LinkAndTextBelow());
                    break;
                case "LinkAndTextAdjacent":
                    renderLinkAndTextAdjacent(new LinkAndTextAdjacent());
                    break;
                default:
                    Console.WriteLine("Link Only");
                    break;
            }
        }

        public void renderLinkAndTextBelow(LinkAndTextBelow link)
        {
            Console.WriteLine("Link and Text Below");
        }

        public void renderLinkAndTextAdjacent(LinkAndTextAdjacent linkAndTextAdjacent)
        {
            Console.WriteLine("Link And Text Adjacent");
        }
    }

    public class Link
    {
        public String linkType;
    }

    public class LinkAndTextBelow : Link
    {
        public LinkAndTextBelow() {
            base.linkType = "LinkAndTextBelow";
        }
    }

    public class LinkAndTextAdjacent : Link
    {
        public LinkAndTextAdjacent() {
            base.linkType = "LinkAndTextAdjacent";
        }
    }
}