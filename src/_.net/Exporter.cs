﻿using System;

namespace CodingKata
{
    interface DocumentExporter
    {
        void pdfExporter();
        void excelExporter();
    }

    public class Exporter : DocumentExporter
    {
        
    public void pdfExporter()
    {
        Console.WriteLine("PDF Exporter");
    }

    public void excelExporter()
    {
        Console.WriteLine("Excel Exporter");
    }

    public static void main(String[] args)
    {
        Exporter exporter = new Exporter();
        exporter.excelExporter();
    }
}

}