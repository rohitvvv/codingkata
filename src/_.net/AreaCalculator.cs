﻿using System;
namespace CodingKata
{
    public class AreaCalculator
    {
        public static int calculateArea(Square s)
        {
            return s.height * s.height;
        }
        public static int calculateArea(Rectangle r)
        {
            return r.height * r.width;
        }
        public static void main(String[] args)
        {
            Rectangle rectangle = new Rectangle();
            rectangle.setHeight(10);
            rectangle.setWidth(20);
            Console.WriteLine(calculateArea(rectangle));

            Square sqaure = new Square();
            sqaure.setHeight(10);
            Console.WriteLine(calculateArea(sqaure));

            Rectangle square_rectangle = new Square();
            square_rectangle.setWidth(5);
            square_rectangle.setHeight(2);
            Console.WriteLine(calculateArea(square_rectangle));

        }
    }

    public class Rectangle
    {
        public int height;
        public int width;

        public void setHeight(int height)
        {
            this.height = height;
        }

        public void setWidth(int width)
        {
            this.width = width;
        }

    }

    public class Square : Rectangle
    {
        public void setWidth(int value)
        {
            this.width = value;
            this.height = value;
        }
        public void setHeight(int value)
        {
            this.height = value;
            this.width = value;
        }
    }
}