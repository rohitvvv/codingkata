package _java.business;


public class AreaCalculator {

    public static int calculateArea(Square s) {
        return s.height * s.height;
    }

    public static int calculateArea(Rectangle r) {
        return r.height * r.width;
    }

    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle();
        rectangle.setHeight(10);
        rectangle.setWidth(20);
        System.out.println(calculateArea(rectangle));

        Square square = new Square();
        square.setHeight(10);
        System.out.println(calculateArea(square));

        Rectangle square_rectangle = new Square();
        square_rectangle.setWidth(5);
        square_rectangle.setHeight(2);
        System.out.println(calculateArea(square_rectangle));

    }
}

class Rectangle {
    public int height;
    public int width;

    public void setHeight(int height) {
        this.height = height;
    }

    public void setWidth(int width) {
        this.width = width;
    }

}

class Square extends Rectangle {

    @Override
    public void setWidth(int value) {
        this.width = value;
        this.height = value;
    }

    @Override
    public void setHeight(int value) {
        this.height = value;
        this.width = value;
    }
}
