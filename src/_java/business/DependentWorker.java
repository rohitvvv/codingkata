package _java.business;

//Lower level class
class Worker {
    public void work() {
        System.out.println("I work hard");
    }
}

//Higher level class
class Manager {
    Worker worker;

    public void setWorker(Worker worker) {
        this.worker = worker;
    }

    public void manage() {
        worker.work();
    }
}

//Yet another low level class.Future requirement.
//Cannot be introduced without making changes to the Manager!!!
class SuperWorker {
    public void work() {
        System.out.println("I am the super worker");
    }
}

public class DependentWorker {
    public static void main(String[] args) {
        Manager manager = new Manager();
        manager.setWorker(new Worker());
        manager.manage();
    }
}
