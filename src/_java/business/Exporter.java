package _java.business;


interface DocumentExporter{
    public void pdfExporter();
    public void excelExporter();
}

public class Exporter implements  DocumentExporter{
    @Override
    public void pdfExporter() {
        System.out.println("PDF Exporter");
    }

    @Override
    public void excelExporter() {
        System.out.println("Excel Exporter");
    }

    public static void main(String[] args) {
       Exporter exporter = new Exporter();
       exporter.excelExporter();
    }
}
