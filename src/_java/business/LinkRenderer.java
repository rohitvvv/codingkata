package _java.business;

public class LinkRenderer {
    void renderLink(String linkType) {
        switch (linkType) {
            case "LinkAndTextBelow":
                renderLinkAndTextBelow(new LinkAndTextBelow());
                break;
            case "LinkAndTextAdjacent":
                renderLinkAndTextAdjacent(new LinkAndTextAdjacent());
                break;
            default:
                System.out.println("Link Only");
        }
    }

    public void renderLinkAndTextBelow(LinkAndTextBelow link) {
        System.out.println("Link and Text Below");
    }

    public void renderLinkAndTextAdjacent(LinkAndTextAdjacent linkAndTextAdjacent) {
        System.out.println("Link And Text Adjacent");
    }
}

class Link {
    public String linkType;
}

class LinkAndTextBelow extends Link {
    LinkAndTextBelow() {
        super.linkType = "LinkAndTextBelow";
    }
}

class LinkAndTextAdjacent extends Link {
    LinkAndTextAdjacent() {
        super.linkType = "LinkAndTextAdjacent";
    }
}