## Coding Kata
Welcome to Coding Kata. Learn to refactor code by applying
different principles like

* SOLID
    * Single Responsibility Principle
    * Open Close Principle
    * Liskov's Substitution Principle
    * Interface Seggregation Principle
    * Dependency Inversion Principle  
* DRY : Don't Repeat yourself
* YAGNI: You ain't gonna need it
* FIRST: Fast, Independent, Repeatable, Self validating, Timely
* Law of Demeter
* Tell don't ask Principle


### File Reference
* Single Responsibility Principle: Employee
* Open/Close Principle: LinkRenderer
* Liskov's Substitution Principle: AreaCalculator
* Interface Seggreation Principle: Exporter
* Dependency Inversion Principle: DependentWorker








